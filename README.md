# Smart Home Thermostat App

A SwiftUI demo project.

<table>
<thead>
  <tr>
      <th colspan="2">
     <div align="center">Demo Video</div>
      </th>
  </tr>
</thead>
<tbody>
  <tr>
     <td>
      ![ ](Screenshots/demo.mp4)
      </td>
  </tr>
</tbody>
</table>
